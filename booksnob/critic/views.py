from critic.forms import ReviewForm
from django.views.generic.edit import FormView
from django.views.generic import TemplateView, DetailView, ListView
from reader.models import Review, Book

class ReviewView(FormView):
	form_class = ReviewForm
	template_name = 'critic/review.html'
	success_url = '/critic/thanks/'
	"""
	def post(self,request, *args, **kwargs):
	"""

	def form_valid(self, form):
		form.send_email()
		text = form.cleaned_data['text']
		book_name = form.cleaned_data['book_name']	
		print book_name	
		book_name = book_name.split('- ')
		print book_name		
		book_name_id = Book.objects.filter(name = book_name[1]).values()
		got_book_name_id=[]
		for i in book_name_id:
			got_book_name_id.append(i['id'])
			print i			
		review = Review.objects.create(
			text = text,
			book_id = got_book_name_id[0],)

		return super(ReviewView, self).form_valid(form)	
	
class ThanksView(TemplateView):
	template_name = 'critic/thanks.html'     

class SeeReviewsList(ListView):
	model = Review	
	template_name = 'critic/see_reviews_list.html'	  

class SeeOneReview(DetailView):
	model = Review
	template_name = 'critic/see_one_review.html'	 
