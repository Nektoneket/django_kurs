"""Dj URL Configuration for taestapp


"""
from django.conf.urls import url


from reader import views

urlpatterns = [
    url(r'^$', views.HomeView.as_view()),
    url(r'^get_author/$', views.Form.as_view(), name='get_author'),
    url(r'^thanks/$', views.ThanksView.as_view(), name='info'),
    url(r'^get_author/(?P<pk>\d+)/$', views.GetAuthorView.as_view()),
    url(r'^get_author/get_list/$', views.GetAuthorList.as_view()),
    url(r'^get_author/get_list/(?P<pk>\d+)/$', views.OneAuthorView.as_view(), name='one_author'),
]
