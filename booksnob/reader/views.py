from django.views.generic import TemplateView, DetailView, ListView
from reader.manager import AuthorManager
from reader.actions import get_books, get_authors
from reader.forms import AuthorForm
from django.http.request import HttpRequest
from django.http import HttpResponse
from reader.models import Author

from django.views.generic.edit import FormView 

class HomeView(TemplateView):
	template_name = 'base.html'
	

class HomeView(TemplateView):
	template_name = 'reader/home.html'

	def get_context_data(self, **kwargs):
		context = super(HomeView,self).get_context_data(**kwargs)
		context['author_name'] = get_authors()
		context['books'] = get_books()

		return context


class Form(FormView):
	form_class = AuthorForm
	template_name = 'reader/get_authors.html'
	success_url = '/reader/thanks/'
	"""
	def post(self,request, *args, **kwargs):
	"""
	def form_valid(self,form):
		self.object=form.save()
		print self.object
		return super(Form, self).form_valid(form)


class ThanksView(TemplateView):
    template_name = 'reader/thanks.html'

class GetAuthorView(DetailView):
	model = Author
	template_name = 'reader/authors.html'
	"""

	def get_context_data(self, **kwargs):
		# Call the base implementation first to get a context
		context = super(GetAuthorView, self).get_context_data(**kwargs)
		# Add in a QuerySet of all the books
		context['book_list'] = Author.objects.all()
		return context
	"""
class OneAuthorView(DetailView):
	model = Author
	template_name = 'reader/one_author.html'



class GetAuthorList(ListView):
	model = Author
	
	template_name = 'reader/get_list.html'

