from reader.models import Book, Author


def get_authors():
	author_name = Author.objects.all()
	return author_name

def get_books():
	final_dict = {}
	for name in get_authors():
		author = name.first_name
		temporary_list = []
		for book in Book.objects.filter(author=name):
			temporary_list.append(book.name)
		final_dict[author]=temporary_list
	return final_dict

def get_books1():

	all_books = Book.objects.all()
	return all_books	

