from django.db import models
from reader.manager import AuthorManager

class Author(models.Model):
	first_name=models.CharField(max_length=30)
	last_name = models.CharField(max_length=40)
	email = models.EmailField()
	filter_author = AuthorManager
	objects = models.Manager
	
	def __unicode__(self):
 		return '{0} {1}'.format(self.first_name, self.last_name)


class Book(models.Model):
	author = models.ForeignKey(Author)
	name = models.CharField(max_length=100)

	def __unicode__(self):
		return '{0} - {1}'.format(self.author.last_name, self.name)

class Review(models.Model):
	book = models.ForeignKey(Book)	
	text = models.CharField(max_length=1000)
	def __unicode__(self):
		return '{0} - {1}'.format(self.book.name, self.text)

class Reader(models.Model):
	book = models.ManyToManyField(Book)
	first_name = models.CharField(max_length = 50)
	last_name = models.CharField(max_length = 50)

	def __unicode__(self):
		return '{0} - {1}'.format(self.name, self.book.name )

		


